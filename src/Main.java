import java.util.Scanner;

public class Main {

    //        Grocery Shopping Application
//        Create class Product, it should contain at least two fields – name and price.
//                Create an empty array of Products – it's size should be at least 5.
//        Populate the array with some products - this array represents the menu for the user.
//        Show this menu to the user.
//        1. Show list of products -> show menu of products and ask user to choose a product to add in his cart. After he bought a product, we should ask the customer if he want to buy a new product.If his answer is y, show the menu product again and repeat until his answer is n, or his cart is full.
//        2. Show cart ->  show customer shopping cart.
//        3. Show total price -> show total price that the customer has to pay until he choose this option.
//        4. Checkout and pay -> show the cart and the price, the customer has to pay for all products he bought. After that his cart will be empty and he could buy more products.
//        5. Leave the shop
//        The user(Client) will choose an option from the menu until he will choose the 5 option. When he choose option 5 (leave the shop),client should be asked if is sure he wants to leave. If his answer is y then show
//        a goodbye message and the app will finish. If the answer is n, then we should display the menu again and have the possibility to choose a menu option and repeat this process.

    public static void main(String[] args) {
        //Aici introducem cantitatea de produse din magazin ;
        System.out.println(" Enter the quantity  of products the will have our shop! ");
        Scanner scannerNumbers = new Scanner(System.in);
        int totalQuantityOfProducts = scannerNumbers.nextInt();
        //aici introducem cantitatea maxima care se va putea achizitiona de un client;
        System.out.println("Enter the quantity of the products that you can add in your cart ! ");
        int maxQuantityOfProducts = scannerNumbers.nextInt();
        Customer customer = new Customer(maxQuantityOfProducts);
        Shop shop = new Shop(totalQuantityOfProducts, customer);
        shop.populateTheArrayOfProducts();
        shop.startShopingAplication();




    }
}
