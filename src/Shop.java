import java.util.Scanner;

public class Shop {
    private Product[] products;
    private int nrOfProductsInShop;
    private Customer customer;
    private Scanner scannerText = new Scanner(System.in);
    private Scanner scannerNumbers = new Scanner(System.in);
    private int remaingProducts;

    public Shop(int nrOfProductsInShop, Customer customer) {
        this.nrOfProductsInShop = nrOfProductsInShop;
        this.customer = customer;
        products = new Product[nrOfProductsInShop];
        remaingProducts = customer.getMaxQuantityOfProductsToBuy();
    }
    //popularea arayului de produse din magazin ;

    public void populateTheArrayOfProducts() {
        int enumeration = 1;
        for (int i = 0; i < products.length; i++) {
            System.out.println(" Ad the product number " + enumeration + " ");
            System.out.println("Please introduce the name: ");
            String name = scannerText.nextLine();
            System.out.println("Please introduce the price of the product: ");
            double price = scannerNumbers.nextDouble();
            Product product1 = new Product(name, price);
            products[i] = product1;
            enumeration++;

        }

    }

    public void showProducts() {
        System.out.println("The products are: ");
        for (int i = 0; i < products.length; i++) {
            Product thisProduct = products[i];
            System.out.println((i + 1) + ". Name " + thisProduct.getName() + " Price " + thisProduct.getPrice());
        }
        chooseTheProduct();
    }

    public void chooseTheProduct() {
        //informam clientul despre cate produse va achizitiona ;
        System.out.println("You can get the maximum " + customer.getMaxQuantityOfProductsToBuy() + " products ");
        String useAnswer;
        do {
            System.out.println("You can buy " + remaingProducts + "more products!");
            System.out.println("Choose a product:  ");
            int productSelected = scannerNumbers.nextInt();
            customer.addProductInCart(products[productSelected - 1]);
            //dupa fiecare produs adaugat cartul scade un produs din capacitate;
            remaingProducts--;
            if (remaingProducts == 0) {
                System.out.println(" The cart is full , please check out and pay! ");
                useAnswer = "n";
                checkOutAndPay();
            } else {
                System.out.println("Do you want to add another product in your cart ? (y)/(n) ");
                useAnswer = scannerText.nextLine();
            }
        } while (remaingProducts != 0 && useAnswer.equalsIgnoreCase("y"));
    }

    public void showMenu() {
        System.out.println();
        System.out.println("Menu: ");
        System.out.println("1.Show list of products.");
        System.out.println("2.Show cart.");
        System.out.println("3.Show total price.");
        System.out.println("4.Checkout and Pay.");
        System.out.println("5.Leave the Shop.");
    }

    public void startShopingAplication() {
        int option;
        String userAnswer = "n";
        do {
            showMenu();
            System.out.println("Please choose an option from this menu: ");
            option = scannerNumbers.nextInt();

            switch (option) {
                case 1:
                    if (!customer.cartIsFull()) {
                        showProducts();
                    } else {
                        System.out.println("Your cart is full please check out and pay !");
                    }
                    checkOutAndPay();
                    break;
                case 2:
                    customer.showCart();
                    break;
                case 3:
                    customer.showTotalPrice();
                    if (customer.getCartValue() == 0.0) {
                        System.out.println("You have nothing to pay ");
                    } else {
                        System.out.println("You have to pay " + customer.getCartValue() + "RON now");
                    }
                    break;
                case 4:
                    checkOutAndPay();
                    break;
                case 5:
                    System.out.println("Are you sure you want to exit? (y/n)");
                    userAnswer = scannerNumbers.nextLine();
                    if (userAnswer.equalsIgnoreCase("y")) {
                        System.out.println("Thanks for shoping with us!  ");
                    }
                    break;
                default:
                    System.out.println("The option is invalid ! ");

            }

        } while (!userAnswer.equalsIgnoreCase("n"));

    }

    public  void checkOutAndPay() {
        customer.showTotalPrice();
        if (customer.getCartValue() == 0.0) {
            System.out.println("You have nothing to pay");
            return;
        }
        System.out.println("You have to pay " + customer.getCartValue() + " RON! ");
        System.out.println("Enter the amount you want to pay : ");
        double paydAmount = scannerNumbers.nextDouble();
        double remainingAmount;
        if (paydAmount == customer.getCartValue()) {
            System.out.println("Thank you for your purchase ");
        } else if (paydAmount < customer.getCartValue()) {
            do {
                remainingAmount = customer.getCartValue() - paydAmount;
                System.out.println("You still have to pay " + remainingAmount);
                System.out.println("Please enter amount again: ");
                paydAmount = scannerNumbers.nextDouble();
                if (paydAmount > remainingAmount) {
                    System.out.println("Please take " + (paydAmount - remainingAmount) + "RON back! ");
                }
                customer.setCartValue(remainingAmount);

            } while (remainingAmount > paydAmount);
        } else {
            System.out.println("Please take back " + (paydAmount - customer.getCartValue()) + "RON back! ");
        }
        remaingProducts = 0;
        customer.setCartValue(0.0);
        customer.clearCart();
    }

    public Product[] getProducts() {
        return products;
    }

    public void setProducts(Product[] products) {
        this.products = products;
    }

    public int getNrOfProductsInShop() {
        return nrOfProductsInShop;
    }

    public void setNrOfProductsInShop(int nrOfProductsInShop) {
        this.nrOfProductsInShop = nrOfProductsInShop;
    }

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }
}
