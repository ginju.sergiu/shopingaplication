public class Customer {
    private int maxQuantityOfProductsToBuy;
    private Product [] cart;
    private double cartValue;

    public Customer(int maxQuantityOfProducts) {
        this.maxQuantityOfProductsToBuy = maxQuantityOfProducts;
        cart = new Product[maxQuantityOfProducts];
    }
    public void showCart(){
        if(cart[0] == null){
            System.out.println("Your cart is empty! ");
        }else {
            int contor = 0;
            for (int i = 0; i < cart.length; i++) {
                if(cart[i] != null){
                    System.out.println(contor + " Name: "+ cart[i].getName()+ " Price: "+cart[i].getPrice());
                    contor++;
                }
            }
        }
    }
    
    public void addProductInCart(Product productToAdd){
       int counting = 0;
        for (int i = 0; i < cart.length; i++) {
            if (cart[i] == null) {
                counting = i;
                break;
            }

        }
        cart[counting] = productToAdd;
        System.out.println("You succesfully added "+ productToAdd.getName()+" to your cart ! ");
    }
    public void showTotalPrice(){
        cartValue = 0.0;
        if(cart[0]!=null){
            for (int i = 0; i < cart.length; i++) {
               if(cart[i] != null){
                   cartValue = cartValue+cart[i].getPrice();
               }
            }
        }
    }

public boolean cartIsFull(){
        boolean cartFull =false;
        int contor = 0;
    for (int i = 0; i < cart.length; i++) {
        if(cart[i] != null) {
            contor++;
        }
    }
    if(contor == cart.length){
        cartFull = true;
    }
    return cartFull;
}
public void clearCart(){
    for (int i = 0; i < cart.length; i++) {
        cart[i] = null;
    }
}


    public int getMaxQuantityOfProductsToBuy() {
        return maxQuantityOfProductsToBuy;
    }

    public void setNrMaxOfProductsInCart(int maxQuantityOfProducts) {
        this.maxQuantityOfProductsToBuy = maxQuantityOfProducts;
    }

    public Product[] getCart() {
        return cart;
    }

    public void setCart(Product[] cart) {
        this.cart = cart;
    }

    public double getCartValue() {
        return cartValue;
    }

    public void setCartValue(double cartValue) {
        this.cartValue = cartValue;
    }
}
